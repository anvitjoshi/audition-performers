package com.audition.core;

public abstract class ObjectFactory {
	
	public static DoPerform getObject(String auditionerType) {
		switch(auditionerType) {
			case "performer":
				return new Performer();
			case "dancer":	
				return new Dancer();
			case "vocalist":	
				return new Vocalist();
			default:
				return null;
		}
	}
}
