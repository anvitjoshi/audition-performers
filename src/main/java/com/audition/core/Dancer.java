package com.audition.core;

public class Dancer implements DoPerform {
	
	public String perform() {
		return "No one is dancing at this moment";
	}
	
	public String perform(int id, String style) {
		return style + " - " + id + " - dancer";
	}

}
