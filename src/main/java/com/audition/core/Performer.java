package com.audition.core;

public class Performer implements DoPerform {
	
	public String perform() {
		return "No one is performing at this moment";
	}
	
	public String perform(int id) {
		return id + " - performer";
	}

}
