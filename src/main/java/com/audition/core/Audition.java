package com.audition.core;

public class Audition {
	

	private String auditionerType;
	private int unionId;
	private String style = null;
	private char key = '\0';
	private int volume = -1;
	
	public Audition(int id) {
		this.unionId = id;
		this.auditionerType = "performer";
	}
	
	public Audition(int id, String style) {
		this.unionId = id;
		this.style = style;
		this.auditionerType = "dancer";
	}
	
	public Audition(int id, char key) {
		this.unionId = id;
		this.key = key;
		this.auditionerType = "vocalist";
	}
	
	public Audition(int id, char key, int volume) {
		if(volume < 1 || volume > 10) {
			throw new IllegalArgumentException("Volume must be between 1 and 10");
		}
		this.unionId = id;
		this.key = key;
		this.volume = volume;
		this.auditionerType = "vocalist";
	}
	
	public int getUnionId() {
		return unionId;
	}
	
	public String getStyle() {
		return style;
	}
	
	public char getKey() {
		return key;
	}
	
	public String getType() {
		return auditionerType;
	}
	
	public int getVolume() {
		return volume;
	}
	
}
