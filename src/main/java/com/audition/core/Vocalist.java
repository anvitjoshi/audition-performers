package com.audition.core;

public class Vocalist implements DoPerform {
	
	public String perform() {
		return "No one is performing at this moment";
	}
	
	public String perform(int id, char key) {
		return "I sing in the key of - " + key + " - " + id;
	}
	
	public String perform(int id, char key, int volume) {
		return "I sing in the key of - " + key + " - " +"at the volume "+ volume + " - " + id;
	}
}
