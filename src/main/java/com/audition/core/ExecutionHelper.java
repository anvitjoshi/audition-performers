package com.audition.core;

import java.util.List;

public class ExecutionHelper {
	
	public int getAllAuditioners(List<Audition> auditioners) {
		return auditioners.size();
	}

	public int getNumOfPerformers(List<Audition> auditioners) {
		int count = 0;
		for (Audition a: auditioners) {
			if(a.getType().equalsIgnoreCase("performer"))
			count ++;
		}
		return count;
	}

	public int getNumOfDancers(List<Audition> auditioners) {
		int count = 0;
		for (Audition a: auditioners) {
			if(a.getType().equalsIgnoreCase("dancer"))
			count ++;
		}
		return count;
	}

	public int getNumOfVocalists(List<Audition> auditioners) {
		int count = 0;
		for (Audition a: auditioners) {
			if(a.getType().equalsIgnoreCase("vocalist"))
			count ++;
		}
		return count;
	}
	
	public String conductAudition(Audition a) {
		
		String output = null;
		DoPerform dp = ObjectFactory.getObject(a.getType());
		
		if(a.getStyle() == null && a.getKey() == '\0') {
			output = ((Performer) dp).perform(a.getUnionId());
		}
		else if(a.getStyle() != null) {
			output = ((Dancer) dp).perform(a.getUnionId(), a.getStyle());
		}
		
		else if(a.getKey() != '\0') {
			if(a.getVolume() > 0) {
				output = ((Vocalist) dp).perform(a.getUnionId(), a.getKey(), a.getVolume());
			}
			else {
				output = ((Vocalist) dp).perform(a.getUnionId(), a.getKey());
			}
		}
		
		return output;
	}
	
}
