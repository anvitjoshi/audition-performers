package com.audition.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PerformerTest {
	
	private Audition p1;
	private ExecutionHelper eh;
	
	
	@Before
	public void setUp() {
		eh = new ExecutionHelper();
		p1 = new Audition(101);
	}
	
	@Test
	public void performerTest() {
		String expected = "101 - performer";
		String actual = eh.conductAudition(p1);
		assertEquals(expected, actual);
	}

}
