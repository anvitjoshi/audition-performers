package com.audition.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VocalistTest {

	private Audition v1;
	private Audition v2;
	private ExecutionHelper eh;
	
	@Rule public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void setUp() {
		eh = new ExecutionHelper();
		v1 = new Audition(301, 'G');
		v2 = new Audition(302, 'G', 5);
	}
	
	@Test
	public void vocalistTest() {
		String expected = "I sing in the key of - G - 301";
		String actual = eh.conductAudition(v1);
		assertEquals(expected, actual);
	}
	
	@Test
	public void vocalistVoiceTest() {
		String expected = "I sing in the key of - G - at the volume 5 - 302";
		String actual = eh.conductAudition(v2);
		assertEquals(expected, actual);
	}
	
	@Test
	public void lowVolumeTest() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Volume must be between 1 and 10");
		new Audition(302, 'G', 0);
	}
	
	@Test
	public void highVolumeTest() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Volume must be between 1 and 10");
		new Audition(302, 'G', 11);
	}

}
