package com.audition.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DancerTest {

	private Audition p1;
	private ExecutionHelper eh;
	
	
	@Before
	public void setUp() {
		eh = new ExecutionHelper();
		p1 = new Audition(201, "tap");
	}
	
	@Test
	public void dancerTest() {
		String expected = "tap - 201 - dancer";
		String actual = eh.conductAudition(p1);
		assertEquals(expected, actual);
	}

}
