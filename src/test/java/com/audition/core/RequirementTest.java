package com.audition.core;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class RequirementTest {
	private List<Audition> auditioners;
	private ExecutionHelper eh;
	
	@Before
	public void setUp() {
		
		auditioners = new ArrayList<>();
		eh = new ExecutionHelper();
		
		Audition p1 = new Audition(101);
		Audition p2 = new Audition(102);
		Audition p3 = new Audition(103);
		Audition p4 = new Audition(104);
		
		Audition d1 = new Audition(201, "tap");
		Audition d2 = new Audition(202, "hip hop");
		
		Audition v1 = new Audition(301, 'G');
		
		auditioners.add(p1);
		auditioners.add(p2);
		auditioners.add(p3);
		auditioners.add(p4);
		auditioners.add(d1);
		auditioners.add(d2);
		auditioners.add(v1);
	}
	
	@Test
	public void getAllAuditionersTest() {
		int expected = 7;
		int actual = eh.getAllAuditioners(auditioners);
		assertEquals(expected, actual);
	}
	
	@Test
	public void getNumOfPerformersTest() {
		int expected = 4;
		int actual = eh.getNumOfPerformers(auditioners);
		assertEquals(expected, actual);
	}
	
	@Test
	public void getNumOfDancersTest() {
		int expected = 2;
		int actual = eh.getNumOfDancers(auditioners);
		assertEquals(expected, actual);
	}
	
	@Test
	public void getNumOfVocalistsTest() {
		int expected = 1;
		int actual = eh.getNumOfVocalists(auditioners);
		assertEquals(expected, actual);
	}

}
